package cav.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import cav.utils.Dmu;

public class AllFieldModel extends AbstractTableModel  {
	private ArrayList rows=new ArrayList();
	private ResultSet rs;
	private int columnCount;
	private String[] columnName={"Название поля","Название поля на форме","Тип поля","Кол.-во значений"};	
	
	public AllFieldModel(){
		super();
		refresh();		
	}
	
	public void refresh(){
		rows.clear();
		String sql="select * from field_all_temp order by field_name";
		rs = Dmu.getQueryRes(sql);
		try {
			columnCount=rs.getMetaData().getColumnCount();
			while (rs.next()){
				ArrayList row=new ArrayList();
				for (int i=1;i<=columnCount;i++){
					row.add(rs.getObject(i));
				}
				rows.add(row);
			}
            rs.close();
            this.fireTableDataChanged();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public ArrayList getRow(int row){
		return (ArrayList) rows.get(row);
	}	
	
	public int getRowCount() {
		return rows.size();
	}

	public int getColumnCount() {
		return columnCount;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		ArrayList row= (ArrayList) rows.get(rowIndex);
		return row.get(columnIndex);
	}
	
    public String getColumnName(int columnIndex){
        return columnName[columnIndex];
    }
/*    
    public Class getColumnClass(int c){
        return ((ArrayList)rows.get(0)).get(c).getClass();

    }	
*/
    // добавляет сроку в конец массива
    public void setNewRow(ArrayList data){
    	rows.add(data);
    	this.fireTableDataChanged();	
    }
    // заменяет строку с rec_id 
    public void setRow(ArrayList data,int rec_id){
    	rows.set(rec_id,data);
    	this.fireTableDataChanged();
    }
	
    public boolean isCellEditable(int r,int c){
    	if (c>=1 && c<3) return true;
		return false;
    }
    public void setValueAt(Object value,int rowIndex,int columnIndex){
    	synchronized (rows){
    		((ArrayList<Object>) rows.get(rowIndex)).set(columnIndex,value);
    	}
    }    
}
