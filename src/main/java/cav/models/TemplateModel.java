package cav.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import cav.utils.Dmu;

public class TemplateModel extends AbstractTableModel {
	
	private ArrayList rows=new ArrayList();
	private ResultSet rs;
	private int columnCount;
	private String[] columnName={"#","Название","Дата документа"};

	public TemplateModel() {
		super();
		refresh();
	}	
	
	public void refresh(){
		rows.clear();
		String sql="select * from templ";
		rs = Dmu.getQueryRes(sql);
		try {
			columnCount=rs.getMetaData().getColumnCount();
			while (rs.next()){
				ArrayList row=new ArrayList();
				for (int i=1;i<=columnCount;i++){
					row.add(rs.getObject(i));
				}
				rows.add(row);
			}
            rs.close();
            this.fireTableDataChanged();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList getRow(int row){
		return (ArrayList) rows.get(row);
	}
	
	public int getRowCount() {
		// TODO Auto-generated method stub
		return rows.size();
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		ArrayList row= (ArrayList) rows.get(rowIndex);
		return row.get(columnIndex);
	}

    public String getColumnName(int columnIndex){
        return columnName[columnIndex];
    }
    public Class getColumnClass(int c){
        return ((ArrayList)rows.get(0)).get(c).getClass();

    }
}
