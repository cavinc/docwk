package cav.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import cav.utils.Dmu;

public class SpecFieldModel extends AbstractTableModel {
	private ArrayList rows=new ArrayList();
	private ResultSet rs;
	private int columnCount;
	private String[] columnName={"№","Значение"};	
	
	public SpecFieldModel(String field_name){
		super();
		refresh(field_name);
	}
	
	public void refresh(String field_name){
		rows.clear();
		String sql="select id_rec,field_source from field_all_spec where field_name = '"+field_name+"'";
		rs = Dmu.getQueryRes(sql);
		try {
			columnCount=rs.getMetaData().getColumnCount();
			while (rs.next()){
				ArrayList row=new ArrayList();
				for (int i=1;i<=columnCount;i++){
					row.add(rs.getObject(i));
				}
				rows.add(row);
			}
            rs.close();
            this.fireTableDataChanged();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public ArrayList getRow(int row){
		return (ArrayList) rows.get(row);
	}
	
	public int getRowCount() {
		return rows.size();
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnCount;
	}
	
    public String getColumnName(int columnIndex){
        return columnName[columnIndex];
    }

	public Object getValueAt(int rowIndex, int columnIndex) {
		ArrayList row= (ArrayList) rows.get(rowIndex);
		return row.get(columnIndex);
	}
	
	// добавляет новую строку
	public void setNewRow(String data){
		int max_row = 0;
		if (rows.size()!=0) {
			max_row=(Integer) ((ArrayList)rows.get(rows.size()-1)).get(0)+1;
		}
		ArrayList row=new ArrayList();
		row.add(max_row);
		row.add(data);
		rows.add(row);
		this.fireTableDataChanged();
	}
	// изменяем строку
	public void setRow(String data,int rec_id){
		((ArrayList) rows.get(rec_id)).set(1, data);
		this.fireTableDataChanged();
	}
	// удаляем строку 
	public void delRow(int rec_id){
		rows.remove(rec_id);
		this.fireTableDataChanged();
	}
	/*
    public boolean isCellEditable(int r,int c){
    	if (c>=1) return true;
		return false;
    }
	
    public void setValueAt(Object value,int rowIndex,int columnIndex){
    	synchronized (rows){
    		((ArrayList<Object>) rows.get(rowIndex)).set(columnIndex,value);
    	}
    } 
	*/

}
