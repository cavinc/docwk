package cav.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import cav.utils.Dmu;

public class TemplatePrefModel extends AbstractTableModel{
	private ArrayList rows=new ArrayList();
	private ResultSet rs;
	private int columnCount;
	private String[] columnName={"Название поля","Название поля на форме","Тип поля"};
	
	public TemplatePrefModel(int docId){
		super();
		refresh(docId);
	}
	public void refresh(int docId){
		rows.clear();
		// TODO Исправить
		String sql="select field_name,field_form_name,field_type,id from pref_templ where id="+Integer.toString(docId); 
		rs = Dmu.getQueryRes(sql);
		try {
			columnCount=rs.getMetaData().getColumnCount();
			while (rs.next()){
				ArrayList row=new ArrayList();
				for (int i=1;i<=columnCount;i++){
					row.add(rs.getObject(i));
				}
				rows.add(row);
			}
            rs.close();
            this.fireTableDataChanged();			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public ArrayList getRow(int row){
		return (ArrayList) rows.get(row);
	}
	
	public int getRowCount() {
		// TODO Auto-generated method stub
		return rows.size();
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		ArrayList row= (ArrayList) rows.get(rowIndex);
		return row.get(columnIndex);
	}
	
    public String getColumnName(int columnIndex){
    	return columnName[columnIndex];
    }
    
 /*  
    public Class getColumnClass(int c){
        return ((ArrayList)rows.get(0)).get(c).getClass();
    }
*/    
    public boolean isCellEditable(int r,int c){
    	if (c>=1) return true;
		return false;
    }
    
    public void setValueAt(Object value,int rowIndex,int columnIndex){
    	synchronized (rows){
    		((ArrayList<Object>) rows.get(rowIndex)).set(columnIndex,value);
    	}
    }

}
