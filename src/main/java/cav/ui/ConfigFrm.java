package cav.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cav.utils.Glu;

public class ConfigFrm extends JDialog implements ActionListener {
	private static final long serialVersionUID = -4124339159198246188L;
	private int w=400;
	private int h=210;
	
	
	private JTextField tfTemplatePath= new JTextField(); 
	private JTextField tfBaseFile = new JTextField();
	private JTextField tfOutTemplatePath = new JTextField();
	
	private JTextField tfPathInIReport = new JTextField();
	
	private JTextField tfOfficeBinPath = new JTextField();
	
	public ConfigFrm(){
		super();
		this.setTitle("Настройки");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(w,h);
		JPanel upPanel = new JPanel();
		upPanel.setLayout(new BoxLayout(upPanel,BoxLayout.PAGE_AXIS));
		upPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		upPanel.add(new JLabel("Расположение файла базы"));
		upPanel.add(tfBaseFile);
		
		upPanel.add(new JLabel("Путь к шаблонам"));
		upPanel.add(selectPathOrFile(tfTemplatePath,"selButton"));
		upPanel.add(new JLabel("Путь к каталогу подготовленного к печати документа"));
		upPanel.add(selectPathOrFile(tfOutTemplatePath,"selButtonOut"));
		upPanel.add(new JLabel("Путь к исполняемого файлу iReport"));
		upPanel.add(selectPathOrFile(tfPathInIReport,"selIReport"));
		if (System.getProperty("os.name").indexOf("Windows")!=-1){
			upPanel.add(new JLabel("Путь к исполняемогу файлу MS Word или другому текстовому редактору"));
			upPanel.add(selectPathOrFile(tfOfficeBinPath,"winword"));
		}
	
		this.add(upPanel, BorderLayout.CENTER);
		
		JPanel buttonBox = new JPanel();
		buttonBox.setLayout(new BoxLayout(buttonBox,BoxLayout.LINE_AXIS));
		buttonBox.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		JButton runButton = new JButton ("Принять");
		runButton.setName("run");
		runButton.addActionListener(this);
		JButton closeButton = new JButton ("Отмена");
		closeButton.setName("close");
		closeButton.addActionListener(this);
		
		buttonBox.add(Box.createHorizontalGlue());
		buttonBox.add(runButton);
		buttonBox.add(Box.createHorizontalGlue());
		buttonBox.add(closeButton);
		buttonBox.add(Box.createHorizontalStrut(24));
		
		setData(); // заполняем поля
		
		this.add(buttonBox,BorderLayout.SOUTH);
		this.pack();
		this.setModal(true);
		this.setResizable(false);
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));		
	}
	
	private JPanel selectPathOrFile(Object field,String buttonName){
		JPanel pnl = new JPanel();
		pnl.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		//pnl.setBackground(Color.CYAN);
		c.fill = GridBagConstraints.BOTH;
		c.gridx=0;c.gridy=0;c.gridwidth=2;c.weightx=0.85;
		pnl.add((Component) field,c);
		JButton selButton = new JButton("...");
		selButton.addActionListener(this);
		selButton.setName(buttonName);
		c.gridx=2;c.weightx=0.15;
		pnl.add(selButton,c);
		return pnl;
	}
	
	private void setData(){
		tfBaseFile.setText(Glu.getCFG("BASE_TEMPL"));
		tfTemplatePath.setText(Glu.getCFG("TEMPLATE_DIR"));
		tfOutTemplatePath.setText(Glu.getCFG("TEMPLATE_OUT_DIR"));
		tfPathInIReport.setText(Glu.getCFG("IREPORT_PATH"));
		tfOfficeBinPath.setText(Glu.getCFG("OFFICE_BIN_PATH"));
	}

	public void actionPerformed(ActionEvent ae) {
		if (((JButton) ae.getSource()).getName().equals("close")){
			this.dispose();
		}
		if (((JButton) ae.getSource()).getName().equals("run")){
			try {
				Glu.saveCFG(Glu.getSession("UserHome")+File.separator+".docwk"+File.separator+"docwk.conf");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.dispose();
		}		
		if (((JButton) ae.getSource()).getName().equals("selButton")){
			JFileChooser dialog = new JFileChooser();
			dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			dialog.setDialogTitle("Выберите каталог с шаблонами");
			dialog.setApproveButtonText("Выбрать");
			if (dialog.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
				tfTemplatePath.setText(""+dialog.getSelectedFile());
				Glu.setCFG("TEMPLATE_DIR",dialog.getSelectedFile().toString());
			}			
		}
		if (((JButton) ae.getSource()).getName().equals("selButtonOut")){
			JFileChooser dialog = new JFileChooser();
			dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			dialog.setDialogTitle("Выберите каталог с готовыми документами");
			dialog.setApproveButtonText("Выбрать");
			if (dialog.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
				tfOutTemplatePath.setText(""+dialog.getSelectedFile());
				Glu.setCFG("TEMPLATE_OUT_DIR",dialog.getSelectedFile().toString());
			}			
		}	
		if (((JButton) ae.getSource()).getName().equals("selIReport")){
			JFileChooser dialog = new JFileChooser();
			dialog.setDialogTitle("Выберите место нахождения iReport");
			dialog.setApproveButtonText("Выбрать");
			if (dialog.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
				tfPathInIReport.setText(""+dialog.getSelectedFile());
				Glu.setCFG("IREPORT_PATH",dialog.getSelectedFile().toString());
			}			
		}
		if (((JButton) ae.getSource()).getName().equals("winword")){
			JFileChooser dialog = new JFileChooser();
			dialog.setDialogTitle("Выберите место нахождения MS Word");
			dialog.setApproveButtonText("Выбрать");
			if (dialog.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
				tfOfficeBinPath.setText(""+dialog.getSelectedFile());
				Glu.setCFG("OFFICE_BIN_PATH", dialog.getSelectedFile().toString());
			}
		}
		
	}
	

}
