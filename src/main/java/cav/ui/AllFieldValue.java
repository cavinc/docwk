package cav.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;

import com.michaelbaranov.microba.calendar.CalendarPane;
import com.michaelbaranov.microba.calendar.DatePicker;

import cav.models.SpecFieldModel;
import cav.utils.ConstantManager;

/**
 * 
 * @author Kotov Alexandr
 * 
 *  Форма для работы с значениями общих полей
 *
 */
public class AllFieldValue extends JDialog implements ActionListener,ListSelectionListener {

	private static final long serialVersionUID = 405031838651557212L;

	private JTable grid;
	
	private int w = 600;
	private int h = 400;
	
	private JComponent ftSource;
	
	public AllFieldValue(JFrame frm,String field_name,String field_type,int mode){
		super(frm,"Предустановленные значения полей");
		this.setSize(w, h);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		JPanel pnl = new JPanel();
		pnl.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		pnl.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		grid  = new JTable(new SpecFieldModel(field_name));
		setSizeColumn(grid);
		grid.setShowGrid(true);
		grid.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		grid.getSelectionModel().addListSelectionListener(this);
		
		JScrollPane sp=new JScrollPane(grid);
		c.gridx=0;c.gridy=0;c.weightx=4;c.weighty=12;
		c.anchor=GridBagConstraints.NORTH;
		c.fill=GridBagConstraints.BOTH;
		pnl.add(sp,c);
		//
		if (mode==ConstantManager.EDIT_MODE_SPEC) {
			if (field_type.equals("Строка")){
				ftSource = new JTextField();
				c.gridy=13;c.weighty=1;
				c.anchor=GridBagConstraints.SOUTH;
				c.fill=GridBagConstraints.HORIZONTAL;
				pnl.add(ftSource,c);

			}
			if (field_type.equals("Текст")){
				ftSource = new JTextArea(60,3);
				JScrollPane spl = new JScrollPane(ftSource);
				c.gridy=10;c.weighty=4;
				//c.anchor=GridBagConstraints.SOUTH;
				c.fill=GridBagConstraints.BOTH;				
				pnl.add(spl,c);
			}
			if (field_type.equals("Число")){}
			if (field_type.equals("Дата")){
				ftSource=new DatePicker();
				c.gridy=13;c.weighty=1;
				c.anchor=GridBagConstraints.SOUTH;
				c.fill=GridBagConstraints.HORIZONTAL;				
				pnl.add(ftSource,c);
			}		
		}
		this.add(pnl,BorderLayout.CENTER);
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		if (mode==ConstantManager.EDIT_MODE_SPEC){
			JButton insButton = new JButton("Добавить");
			insButton.addActionListener(this);
			
			JButton editButton = new JButton("Изменить");
			editButton.addActionListener(this);
			
			JButton delButton = new JButton("Удалить");
			delButton.addActionListener(this);
			
			JButton saveButton = new JButton("Сохранить");
			saveButton.addActionListener(this);
			
			buttonPanel.add(Box.createHorizontalGlue());
			buttonPanel.add(insButton);
			buttonPanel.add(Box.createHorizontalGlue());
			buttonPanel.add(editButton);	
			buttonPanel.add(Box.createHorizontalGlue());
			buttonPanel.add(delButton);		
			buttonPanel.add(Box.createHorizontalGlue());
			buttonPanel.add(saveButton);			
			
		} else {
			JButton selectButton = new JButton("Выбрать");
			selectButton.addActionListener(this);
			buttonPanel.add(Box.createHorizontalGlue());
			buttonPanel.add(selectButton);			
		}
		
		JButton closeButton = new JButton("Отмена");
		closeButton.addActionListener(this);
		

		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(closeButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		
		this.add(buttonPanel,BorderLayout.SOUTH);
		this.setModal(true);
		//this.pack();
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));			
		
	}
	
	// возвращаем набор данных 
	public ArrayList getData(){
		ArrayList row= new ArrayList();
	    SpecFieldModel model = (SpecFieldModel) grid.getModel();
		for (int i=0;i<model.getRowCount();i++){
			row.add(model.getRow(i));
		}
		return row;
	}

	public boolean saveFlg=false;
	public boolean selectFlg=false;
	private ArrayList selectData; 
	
	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Отмена")){
			this.dispose();
		}
		if (ae.getActionCommand().equals("Добавить")){
			((SpecFieldModel) grid.getModel()).setNewRow(getDataInElement());
		}
		if (ae.getActionCommand().equals("Изменить")){
			int row_id = grid.getSelectedRow();
			if (row_id!=-1){
				((SpecFieldModel) grid.getModel()).setRow(getDataInElement(), row_id);
			}else {
				JOptionPane.showMessageDialog(this,"Не выбран элемент для которого устанавливаем значения");
			}
		}
		if (ae.getActionCommand().equals("Сохранить")){
			saveFlg=true;
			this.dispose();
		}
		if (ae.getActionCommand().equals("Удалить")){
			int row_id = grid.getSelectedRow();
			if (row_id!=-1){
				((SpecFieldModel) grid.getModel()).delRow(row_id);
			}else {
				JOptionPane.showMessageDialog(this,"Не выбран элемент для удаления");
			}
		}
		if (ae.getActionCommand().equals("Выбрать")){
			int rec_id = grid.getSelectedRow();
			if (rec_id!=-1){
				selectFlg=true;
				selectData = ((SpecFieldModel) grid.getModel()).getRow(rec_id);
			}
			this.dispose();
		}
	}
	
	public ArrayList getSelectData(){
		return selectData;
	}
	
	private String getDataInElement(){
		String data = null;
		if (ftSource instanceof JTextField){
			data = ((JTextField) ftSource).getText();
			((JTextField) ftSource).setText("");
		}
		if (ftSource instanceof DatePicker){
			data = new SimpleDateFormat("dd.MM.yyyy").format(((DatePicker) ftSource).getDate());
		}
		if (ftSource instanceof JTextArea) {
			data = ((JTextArea) ftSource).getText();
		}
		return data;
	}
	
	private void setDataInElement(ArrayList data){
		if (ftSource instanceof JTextField) {
			((JTextField) ftSource).setText(data.get(1).toString());
		}
		/*
		if (ftSource instanceof DatePicker){
			try {
				((DatePicker) ftSource).setDate((new SimpleDateFormat("dd.MM.yyyy")).parse(data.get(1).toString()));
			} catch (PropertyVetoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		*/
	}
	
	// устанавливает размер колонки
	private void setSizeColumn(JTable grid){
		TableColumnModel model = grid.getColumnModel();
		model.getColumn(0).setMaxWidth(80);
    	model.getColumn(0).setMinWidth(80);
	  //model.getColumn(2).setMaxWidth(100);
	  //model.getColumn(2).setMinWidth(100);
	}

	public void valueChanged(ListSelectionEvent le) {
		if (le.getValueIsAdjusting()){
			if (grid.getSelectedRow()!=-1){
				ArrayList data =((SpecFieldModel) grid.getModel()).getRow(grid.getSelectedRow());
				setDataInElement(data);
			}
		}
	}

}
