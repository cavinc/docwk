package cav.ui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cav.utils.ConstantManager;
import cav.utils.Dmu;
import cav.utils.Glu;
import cav.utils.ManagerTemplateLoad;

public class InsEditTemplateFrm extends JDialog implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1257477311065664598L;

	private int mode=0;
	
	private JTextField tfTemplateName = new JTextField();
	private JTextField tfTemplatePath = new JTextField();
	
	private final int w=500;
	private final int h= 280;
	
	private JFrame frm;
	
	private ArrayList<?> row;
	
	private JButton linkButton;
	
	public InsEditTemplateFrm(JFrame frm,int mode,ArrayList<?> row){
		super(frm);
		this.frm=frm;
		this.row=row;
		greatefrm(mode);
	}
	
	public InsEditTemplateFrm(JFrame frm,int mode){
		super(frm);
		this.frm=frm;
		greatefrm(mode);
	}
	
	private void greatefrm(int mode){
		this.setSize(w,h);
		this.mode=mode;
		JPanel upPanel = new JPanel();
		upPanel.setLayout(new BoxLayout(upPanel,BoxLayout.PAGE_AXIS));
		upPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		upPanel.add(new JLabel("Наменование шаблона"));
		upPanel.add(tfTemplateName);
		upPanel.add(Box.createVerticalGlue());
		upPanel.add(new JLabel("Путь к шаблону"));
		JPanel selTempPath=new JPanel(new GridBagLayout());
		// selTempPath.setBackground(Color.BLUE);  // Тестовая проверка размеров и размещения
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.gridx=0;c.gridy=0;c.gridwidth=2;c.weightx=0.85;
		
		selTempPath.add(tfTemplatePath,c);
		JButton selPathButton = new JButton("...");
		selPathButton.addActionListener(this);
		selPathButton.setName("selpath");	
		//c.fill = GridBagConstraints.BOTH;
		c.gridx=2;c.gridwidth=1;c.weightx=0.15;
		selTempPath.add(selPathButton,c);
		
		upPanel.add(selTempPath);
	
		
		this.add(upPanel, BorderLayout.CENTER);
		// Панель с кнопками
		JPanel buttonPanel=new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		linkButton = new JButton("Настройка полей");
		linkButton.setName("link");
		linkButton.addActionListener(this);
		
		JButton runButton = new JButton("Принять");
		runButton.addActionListener(this);
		runButton.setName("run");
		JButton closeButton = new JButton("Отмена");
		closeButton.addActionListener(this);
		closeButton.setName("close");
		
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(linkButton);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(runButton);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(closeButton);
		buttonPanel.add(Box.createHorizontalStrut(20));
		
		this.add(buttonPanel, BorderLayout.SOUTH);
		
		if (mode==ConstantManager.INSERT_MODE) {
			this.setTitle("Добавить шаблон");
			linkButton.setEnabled(false);
		} else {
			this.setTitle("Редактируем шаблон");
			tfTemplateName.setText((String) row.get(1));
			tfTemplatePath.setText(row.get(3).toString());
			if (tfTemplatePath.getText().length()==0){
				linkButton.setEnabled(false);
			}
		}		
		
		this.pack();
		this.setSize(w, this.getHeight());
		this.setModal(true);
		this.setResizable(false);
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));		
	}

	ArrayList<String> fields;
	
	public void actionPerformed(ActionEvent ae) {
		if (((JButton) ae.getSource()).getName().equals("close")){
			this.dispose();
		}
		if (((JButton) ae.getSource()).getName().equals("run")){
			String modify_data = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			// TODO Переделать на более правильное с точки зрения ООП
			String sql="";
			if (this.mode==ConstantManager.INSERT_MODE) {
				sql="insert into templ (id,templ_name,modify_date,templ_path) "+
			"values ((select COALESCE(max(id)+1,1) from templ),'"+tfTemplateName.getText()+"','"+modify_data+"','"+tfTemplatePath.getText()+"');";
			}
			if (this.mode==ConstantManager.EDIT_MODE) {
				sql="update templ set templ_name='"+tfTemplateName.getText()+"',templ_path='"+tfTemplatePath.getText()+"',modify_date='"+modify_data+"'"+
			" where id="+row.get(0).toString();
			}
			Dmu.executeQuery(sql);
			if (this.mode==ConstantManager.INSERT_MODE && tfTemplatePath.getText().length()!=0){
				int docid= Glu.getLastDocID();
				Glu.fillTable(docid, fields);
			}
			this.dispose();
		}
		if (((JButton) ae.getSource()).getName().equals("selpath")){
			JFileChooser dialog = new JFileChooser();
			dialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
			dialog.setDialogTitle("Выберите каталог с шаблонами");
			dialog.setApproveButtonText("Выбрать");
			dialog.setCurrentDirectory(new File(Glu.getCFG("TEMPLATE_DIR")));
			if (dialog.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
				tfTemplatePath.setText(""+dialog.getSelectedFile());
				//Glu.setCFG("TEMPLATE_DIR",dialog.getSelectedFile().toString());
				//TODO возможно добавить показ окошка обработки
				fields = ManagerTemplateLoad.getArrayField(tfTemplatePath.getText());
				if (fields.size()!=0) {
					fillAllFieldTable(fields);
					if (mode==ConstantManager.EDIT_MODE){
						linkButton.setEnabled(true);
					}
				}
			}
		}
		if (((JButton) ae.getSource()).getName().equals("link")){
			if (tfTemplatePath.getText().length()!=0){
				fields = ManagerTemplateLoad.getArrayField(tfTemplatePath.getText());
				if (fields.size()!=0) {
					LinkFrm linkFrm = new LinkFrm(this.frm,this.row,fields);
					linkFrm.setVisible(true);
					linkFrm.dispose();
				}else {
					JOptionPane.showMessageDialog(null, "В файле шаблона не определены поля");
				}
			}else {
				JOptionPane.showMessageDialog(null, "Не выбран шаблон");
			}
		}
		
	}
	

	private void fillAllFieldTable(ArrayList<String> fields) {
		for (int i=0;i<fields.size();i++){
			String sql="MERGE INTO field_all_temp (field_name) values ('"+fields.get(i).toString()+"')";
			Dmu.executeQuery(sql);			
		}
	}

}
