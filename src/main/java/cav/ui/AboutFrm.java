package cav.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AboutFrm extends JDialog {
	
	private int w = 300;
	private int h = 200;
	
	public AboutFrm(JFrame frm){
		super(frm,"О программе");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setSize(w,h);
		JPanel pnl = new JPanel();
		pnl.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		pnl.setLayout(new BoxLayout(pnl,BoxLayout.PAGE_AXIS));
		pnl.add(new JLabel("Печать и хранение списка шаблонов"));
		
		this.add(pnl,BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		JButton closeButton = new JButton("Закрыть");
		
		closeButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
			
		});
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(closeButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		
		this.add(buttonPanel,BorderLayout.SOUTH);
		this.setModal(true);
		this.setResizable(false);
		//this.pack();
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));		
	}

}
