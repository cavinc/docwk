package cav.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cav.utils.ConstantManager;
import cav.utils.Dmu;

public class InsEditAllFieldFrm extends JDialog implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2554419292036454974L;
	private int w = 400;
	private int h = 220;
	
	private String[] cmModel={"Строка","Текст","Число","Дата"};
	
	private JTextField fieldName = new JTextField();
	private JTextField fieldFormName = new JTextField();
	private JComboBox fieldType = new JComboBox(cmModel);
	
	private ArrayList data;
	
	private JFrame frm;
	
	public InsEditAllFieldFrm(JFrame frm,int mode,ArrayList data){
		super(frm);
		this.frm=frm;		
		this.data=data;
		create(mode);
	}
	private int mode;
	public InsEditAllFieldFrm(JFrame frm,int mode){
		super(frm);
		this.frm=frm;
		create(mode);
	}
	
	private String oldType = null;
	
	private void create(int mode){
		setSize(w,h);
		this.mode=mode;
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		JPanel pnl = new JPanel();
		pnl.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		pnl.setLayout(new BoxLayout(pnl,BoxLayout.PAGE_AXIS));
		pnl.add(new JLabel("Имя поля"));
		pnl.add(fieldName);
		pnl.add(new JLabel("Название поля на форме"));
		pnl.add(fieldFormName);
		pnl.add(new JLabel("Тип поля"));
		pnl.add(fieldType);
		
		if (mode==ConstantManager.EDIT_MODE){
			fieldName.setText((String) data.get(0));
			fieldName.setEditable(false);
			fieldFormName.setText((String) data.get(1));
			fieldType.getModel().setSelectedItem(data.get(2));
			if (data.get(2)!=null){
				oldType=data.get(2).toString();
			}
		}
		
		this.add(pnl,BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		JButton saveButton = new JButton("Сохранить");
		saveButton.addActionListener(this);
		
		JButton closeButton = new JButton("Отмена");
		closeButton.addActionListener(this);

		buttonPanel.add(Box.createHorizontalGlue());		
		buttonPanel.add(saveButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		buttonPanel.add(closeButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		
		this.add(buttonPanel,BorderLayout.SOUTH);
		this.setModal(true);
		this.setResizable(false);
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));			
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Отмена")){
			saveFlg=false;
			this.dispose();
		}
		if (ae.getActionCommand().equals("Сохранить")){
			//saveData();
			if (!fieldType.getSelectedItem().toString().equals(oldType) && this.mode==ConstantManager.EDIT_MODE){
				int res = JOptionPane.showConfirmDialog(this,"Вы изменили тип поля. Все сохраненные данные будут удалены \n"+
				     " Вы уверены что хотите изменить тип поля ?","Редактирование поля",JOptionPane.YES_NO_OPTION);
	    		if (res==JOptionPane.YES_OPTION){
	    			clearSpecField(fieldName.getText());
	    		}else {
	    			return;
	    		}
			}
			saveFlg=true;
			this.dispose();
		}
		
	}
	
	private boolean saveFlg=false;
	public boolean getSaveFlag(){
		return saveFlg;
	}
	
	public ArrayList getData(){
		ArrayList row= new ArrayList();
		row.add(fieldName.getText());
		row.add(fieldFormName.getText());
		row.add(fieldType.getSelectedItem().toString());
		row.add(getCountSpec(fieldName.getText()));
		return row;
	}
	
	private void saveData(){
		/*
		String sql="MERGE INTO field_all_temp (field_name,field_form_name,field_type)"+
		" values ('"+fieldName.getText()+"','"+fieldFormName.getText()+"','"+fieldType.getSelectedItem().toString()+"')";
		Dmu.executeQuery(sql);
		*/
				
	}
	// удалент спецификацию поля
	private void clearSpecField(String field_name){
		String sql="delete from field_all_spec where field_name='"+field_name+"'";
		Dmu.executeQuery(sql);
	}
	// возвращает количество данных в спецификации
	private int getCountSpec(String field_name){
		int result=0;
		String sql="select count(1) as ci from field_all_spec where field_name='"+field_name+"'";
		ResultSet rs= Dmu.getQueryRes(sql);
		try {
			while (rs.next()){
				result = rs.getInt("ci");
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

}
