package cav.ui;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;

import cav.utils.Glu;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JRDesignViewer;
import net.sf.jasperreports.view.JasperDesignViewer;

public class JasperFrm extends JFrame {
	private JRViewer jv;
	private JRDesignViewer jvDesign;
	private JasperDesignViewer jd;
	private JasperReport jr;
	private JasperDesign ds;
	
	public JasperFrm(){
		super();
		this.setSize(800,600);
		this.setLayout(new BorderLayout());
		try {
			//jr = 
			jvDesign = new JRDesignViewer(Glu.getCFG("TEMPLATE_DIR")+File.separator+"Doc.jrxml",true);
			//jd = new JasperDesignViewer(Glu.getCFG("TEMPLATE_DIR")+File.separator+"Doc.jrxml",true);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		add(jvDesign,BorderLayout.CENTER);
		//jd.setVisible(true); 
		
	}

}
