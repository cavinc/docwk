package cav.ui;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusBar extends JPanel {
	protected JPanel[] em;
	protected JLabel[] lb;
	
	protected void setSb(){
		this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));	
	}
	
	public StatusBar(){
		super();
		JPanel pn=new JPanel();
		pn.setBorder(BorderFactory.createLoweredBevelBorder());
		pn.add(new JLabel(" "));
		this.add(pn);
		setSb();		
	}
	public StatusBar(int elem){
		super();
		for (int i=0;i<elem;i++){
			em[i]=new JPanel();
			}
		setSb();
	}
	

    protected JPanel elemPanel(){
	   JPanel ep=new JPanel();
	   return ep;
    }	

}
