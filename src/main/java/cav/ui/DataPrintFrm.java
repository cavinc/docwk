package cav.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import com.michaelbaranov.microba.calendar.DatePicker;

import cav.utils.ConstantManager;
import cav.utils.Dmu;
import cav.utils.Glu;
import cav.utils.ManagerTemplatePrint;
import cav.utils.PanelAndSelectButton;

public class DataPrintFrm extends JDialog implements ActionListener {
	
	public int w = 500;
	public int h = 300;
	
	private ArrayList data;
	
	public DataPrintFrm(JFrame frm,ArrayList data){
		super(frm);
		this.data=data;
		setSize(w,h);
		//this.setMinimumSize(new Dimension(w,h));
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		JPanel upPanel = new JPanel();

		
		this.add(generateForm((Integer)data.get(0)), BorderLayout.CENTER);
		// Панель с кнопками
		JPanel buttonPanel=new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		JButton closeButton = new JButton("Выход");
		closeButton.addActionListener(this);
		JButton printButton = new JButton("Сформировать");
		printButton.addActionListener(this);
		JButton saveButton = new JButton("Сохранить значения");
		
		JButton openOutPathButton = new JButton("Открыть каталог");
		openOutPathButton.setToolTipText("Октрыть каталог выгрузки");
		openOutPathButton.addActionListener(this);
		
		saveButton.addActionListener(this);
		buttonPanel.add(Box.createHorizontalStrut(50));
		buttonPanel.add(printButton);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(openOutPathButton);		
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(saveButton);
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(closeButton);
		buttonPanel.add(Box.createHorizontalStrut(24));
		
		this.add(buttonPanel, BorderLayout.SOUTH);		
		
		this.pack();
		this.setModal(true);
		//this.setSize(w,this.getSize().height);
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));
		
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Выход")){
			this.dispose();
		}
		
		if (ae.getActionCommand().equals("Сформировать")){
			ManagerTemplatePrint.printTemplate((String) this.data.get(3),getDataInField());
		}
		
		if (ae.getActionCommand().equals("Сохранить значения")){
			HashMap map = (HashMap) getDataInField();
			Set<Map.Entry<String, Object>> entrySet = map.entrySet();
			for (Entry<String, Object> entry : entrySet){
				//System.out.println(entry.getKey());
				if (entry.getValue() instanceof String) {
					String val = ((String)entry.getValue()).replaceAll("'","''");
					String sql="MERGE INTO spec_templ (id,field_name,field_source)"+
					    " values ("+(Integer) this.data.get(0)+",'"+entry.getKey()+"','"+val+"')";
					Dmu.executeQuery(sql);
				}
				if (entry.getValue() instanceof Map) {
					Set<Map.Entry<Integer,String>> entryM1 = ((Map) entry.getValue()).entrySet();
					String sql="delete from spec_templ_extend where id="+(Integer) this.data.get(0);
					Dmu.executeQuery(sql); // очищаем список
					for (Entry<Integer,String> enr: entryM1){
						String val = enr.getValue().replaceAll("'","''");
						sql = "MERGE INTO spec_templ_extend (id,field_name,post_id,field_source)"+
						" values ("+(Integer) this.data.get(0)+",'"+
								entry.getKey()+"',"+enr.getKey()+",'"+val+"')";
						Dmu.executeQuery(sql);
					}
				}
			}
			this.dispose();
		}
		if (ae.getActionCommand().equals("Открыть каталог")){
			if (Glu.getCFG("TEMPLATE_OUT_DIR").length()!=0){
				if (System.getProperty("os.name").indexOf("Windows")!=-1){
					try {
						Runtime.getRuntime().exec("explorer.exe \""+Glu.getCFG("TEMPLATE_OUT_DIR")+"\"");
					}catch (Exception e){
						e.printStackTrace();
						JOptionPane.showMessageDialog(this, "Ошибка "+e.getLocalizedMessage());
					}
				}
				if (System.getProperty("os.name").equals("Linux")){
					// Запускаем наутилус хотя наверно надо лочить кнопку или в конфиге
					// определять чем запускаем
					try {
						Runtime.getRuntime().exec("nautilus "+Glu.getCFG("TEMPLATE_OUT_DIR")+"");
					}catch (Exception e){
						e.printStackTrace();
						JOptionPane.showMessageDialog(this, "Ошибка "+e.getLocalizedMessage());
					}					
				}
			}
			
		}
	}
	// получаем данные из базы значения
	private Map getData(Integer docId){
		// Это больше не запрашивается
		//Map allFieldMap = getAllFieldTable();
		
		Map map=new HashMap<String,Object>();
		String sql="select * from spec_templ where id="+Integer.toString(docId);
		ResultSet rs=Dmu.getQueryRes(sql);

		try {
			while(rs.next()){
				if (rs.getString("field_source").length()==0) {
					/*
					if (allFieldMap.containsKey(rs.getString("field_name"))){
						map.put(rs.getString("field_name"),allFieldMap.get(rs.getString("field_name")));
					}
					*/
				}else {
					map.put(rs.getString("field_name"),rs.getString("field_source"));
				}
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		if (map.size()==0 && allFieldMap.size()!=0) {
			// Не заполнены поля заполняем значенимиями из общих значений
			Set<Map.Entry<String, String>> entrySet = allFieldMap.entrySet();
			for (Entry<String, String> entry : entrySet){
				map.put(entry.getKey(),entry.getValue());
			}
		}
		*/
		
		return map;
	}
	// получение данных полей
	private Map getAllFieldTable(){
		Map map = new HashMap<String,Object>();
		String sql="select * from field_all_temp where spec_count!=0";
		ResultSet rs = Dmu.getQueryRes(sql);
		
		try {
			while(rs.next()){
				map.put(rs.getString("field_name"),rs.getString("field_source"));				
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;
	}
	
	// TODO обеденит с getAllFieldTable()
	// получение названий и типов  полей
	private Map getAllFieldFname(){
		Map map = new HashMap<String,Object>();
		String sql="select * from field_all_temp where not field_form_name is null";
		ResultSet rs = Dmu.getQueryRes(sql);
		try {
			while(rs.next()){
				ArrayList<String> ls = new ArrayList<String>();
				ls.add(rs.getString("field_form_name"));
				ls.add(rs.getString("field_type"));				
				map.put(rs.getString("field_name"),ls);				
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return map;
	}
	
	// заполняем значения
	private JPanel pnl;
	private JComponent ftSource;
	
	private JCheckBox multiAdd = new JCheckBox("Добавить несколько значений в поле");
	
	private JPanel generateForm(Integer docId){
		Map allField = getAllFieldFname();
		//Map allField = new HashMap();
		Map map = getData(docId);
		//Map map = new HashMap();
		String sql="select field_name,field_form_name,field_type,id from pref_templ where id="+Integer.toString(docId); 
		ResultSet rs=Dmu.getQueryRes(sql);

		pnl = new JPanel();
		//pnl.setLayout(new GridLayout(0,2,10,15));
		pnl.setLayout(new GridBagLayout());
		pnl.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		GridBagConstraints c = new GridBagConstraints();
		int offsetY = 0;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(0,10,5,0);
		c.weightx=0.4;
		try {
			while(rs.next()){
				if (rs.getString("field_form_name")!=null){
					c.gridx=0;c.gridy=offsetY;
					pnl.add(new JLabel(rs.getString("field_form_name")),c);

					String fieldType=rs.getString("field_type");
					
					if (fieldType.equals("Число")){
						JFormattedTextField field = new JFormattedTextField(15);
				        field.setFormatterFactory(
				                new DefaultFormatterFactory(
				                        new NumberFormatter(new DecimalFormat("#,###.00"))));						
						field.setName(rs.getString("field_name"));
						c.gridx=1;c.gridwidth  = GridBagConstraints.REMAINDER; 
						pnl.add(new PanelAndSelectButton(field,ConstantManager.ONE_ADD),c);	
					}
					if (fieldType.equals("Дата")){
						DatePicker fieldDate=new DatePicker();
						fieldDate.setName(rs.getString("field_name"));
						try {
							if (map.get(rs.getString("field_name"))!=null){
							 fieldDate.setDate((new SimpleDateFormat("dd.MM.yyyy")).parse((String) map.get(rs.getString("field_name"))));
							} 
						} catch (PropertyVetoException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
						}
						c.gridx=1;c.gridwidth=1;c.weightx=0.2;
						pnl.add(new PanelAndSelectButton(fieldDate,ConstantManager.ONE_ADD),c);
						//pnl.add(fieldDate,c);
						
					}	
					if (fieldType.equals("Строка")){
						JPanel xy = new JPanel();
						xy.setLayout(new BoxLayout(xy,BoxLayout.PAGE_AXIS));
						int count_rec = getExtendCount(docId,rs.getString("field_name"));
						if (count_rec==0) {
							xy.add(setStringPanel(rs.getString("field_name"),(String) map.get(rs.getString("field_name")),"+"));
						} else {
							setMultiElement(xy,docId,rs.getString("field_name"),count_rec);	
						}
						
						JScrollPane sp = new JScrollPane(xy);
						c.gridx=1;c.gridheight=2; 
						c.weightx=0.7;c.gridwidth=2;
						sp.setName(rs.getString("field_name"));
						pnl.add(sp,c);
						c.gridheight=1;
						offsetY +=1;
					}	
					if (fieldType.equals("Текст")){
						JTextArea fieldArea = new JTextArea(5,80);
						fieldArea.setLineWrap(true);
						fieldArea.setName(rs.getString("field_name"));
						fieldArea.setText((String) map.get(rs.getString("field_name")));
						JScrollPane sp=new JScrollPane(fieldArea);
						c.gridx=1;c.weightx = 0.7;c.gridwidth=2;
						pnl.add(sp,c);							
					}
					offsetY +=1;
				}else if (allField.containsKey(rs.getString("field_name")) && 
						(String) ((ArrayList) allField.get(rs.getString("field_name"))).get(1) !=null){
					
					c.gridx=0;c.gridy=offsetY;

					pnl.add(new JLabel((String) ((ArrayList) allField.get(rs.getString("field_name"))).get(0)),c);
					String fieldType=(String) ((ArrayList) allField.get(rs.getString("field_name"))).get(1);
					
					if (fieldType.equals("Число")){
						JFormattedTextField field = new JFormattedTextField(15);
				        field.setFormatterFactory(
				                new DefaultFormatterFactory(
				                        new NumberFormatter(new DecimalFormat("#,###.00"))));							
						field.setName(rs.getString("field_name"));
						c.gridx=1;c.gridwidth  = GridBagConstraints.REMAINDER; 
						pnl.add(field,c);							
					}
					if (fieldType.equals("Дата")){
						DatePicker fieldDate=new DatePicker();
						fieldDate.setName(rs.getString("field_name"));
						try {
							if (map.get(rs.getString("field_name"))!=null){
								fieldDate.setDate((new SimpleDateFormat("dd.MM.yyyy")).parse((String) map.get(rs.getString("field_name"))));
							}
						} catch (PropertyVetoException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
						} catch (ParseException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
						}						
						c.gridx=1;c.gridwidth=1;c.weightx=0.2;
						pnl.add(new PanelAndSelectButton(fieldDate,ConstantManager.ONE_ADD),c);	
						//pnl.add(fieldDate,c);						
					}
					if (fieldType.equals("Строка")){
						/*
						JTextField field = new JTextField();
						field.setName(rs.getString("field_name"));
						field.setText((String) map.get(rs.getString("field_name")));
						*/
						/*
						c.gridx=1;//c.gridwidth  = GridBagConstraints.REMAINDER; 
						c.weightx=0.7;c.gridwidth=2;
						pnl.add(new PanelAndSelectButton(field,ConstantManager.ONE_ADD),c);	
						
						JButton addButton = new JButton("+");
						addButton.addActionListener(new MyOnActionListener());
						c.gridx=3;c.gridwidth=1;
						pnl.add(addButton, c);
						*/
						/*
						JPanel x = new JPanel();
						x.setLayout(new BorderLayout());
						x.add(new PanelAndSelectButton(field,ConstantManager.ONE_ADD),BorderLayout.CENTER);
						JButton addButton = new JButton("+");
						addButton.setName(rs.getString("field_name"));
						addButton.addActionListener(new MyOnActionListener(this,pnl));
						x.add(addButton,BorderLayout.EAST);
						*/
						
						JPanel xy = new JPanel();
						xy.setLayout(new BoxLayout(xy,BoxLayout.PAGE_AXIS));
						int count_rec = getExtendCount(docId,rs.getString("field_name"));
						if (count_rec==0) {
							xy.add(setStringPanel(rs.getString("field_name"),(String) map.get(rs.getString("field_name")),"+"));
						} else {
							setMultiElement(xy,docId,rs.getString("field_name"),count_rec);	
						}
						
						JScrollPane sp = new JScrollPane(xy);
						c.gridx=1;c.gridheight=2; 
						c.weightx=0.7;c.gridwidth=2;
						sp.setName(rs.getString("field_name"));
						pnl.add(sp,c);
						c.gridheight=1;
						offsetY +=1;
						//pnl.add(field,c);							
					}
					if (fieldType.equals("Текст")){
						JTextArea fieldArea = new JTextArea(5,80);
						fieldArea.setLineWrap(true);
						fieldArea.setName(rs.getString("field_name"));
						fieldArea.setText((String) map.get(rs.getString("field_name")));
						JScrollPane sp=new JScrollPane(fieldArea);
						sp.setName(rs.getString("field_name"));
						c.gridx=1;c.weightx = 0.7;c.gridwidth=2;
						//pnl.add(sp,c);
						pnl.add(new PanelAndSelectButton(sp,ConstantManager.ONE_ADD),c);
					}
					offsetY +=1;
				}
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
		}
		//JScrollPane sp = new JScrollPane(pnl);
		offsetY +=2;
		c.gridx=0;c.gridy = offsetY;
		//pnl.add(multiAdd, c);// Проблемма с передачей
		return pnl;
	}
	
	private void setMultiElement(JPanel xx,int doc_id,String field_name,int count_rec){
		String sql="select post_id,field_source from spec_templ_extend where id="+Integer.toString(doc_id)
		+" and field_name='"+field_name+"'";
		ResultSet rs=Dmu.getQueryResTwo(sql);
		try {
			int i=1;
			while(rs.next()){
				if (i==count_rec) {
					xx.add(setStringPanel(field_name,rs.getString("field_source"),"+"));					
				}else {
					xx.add(setStringPanel(field_name,rs.getString("field_source"),"- "));
				}
				i+=1;
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Количество элементов дополнительных полей
	private Integer getExtendCount(int doc_id,String field_name){
		String sql="select count(1) as ci from spec_templ_extend where id="+Integer.toString(doc_id)
		+" and field_name='"+field_name+"'";
		ResultSet rs=Dmu.getQueryResTwo(sql);
		int result = 0;
		try {
			while (rs.next()){
				result = rs.getInt("ci");
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		return result;
	}
	
	// создаем строковую панельку 
	private JPanel setStringPanel(String fieldName,String val,String buttonMode){
		JTextField field = new JTextField();
		field.setName(fieldName);
		field.setText(val);
		
		JPanel x = new JPanel();
		x.setLayout(new BorderLayout());
		x.add(new PanelAndSelectButton(field,ConstantManager.ONE_ADD),BorderLayout.CENTER);
		JButton addButton = new JButton(buttonMode);
		addButton.setName(fieldName);
		addButton.addActionListener(new MyOnActionListener(this,pnl));
		x.add(addButton,BorderLayout.EAST);
		return x;
    }
	
	// получаем данные из полей 
	private Map getDataInField(){
		//System.out.println(pnl.getComponentCount());
		Map fieldsParam = new HashMap();
		for (int i=0;i<pnl.getComponentCount();i++){
			// если компонент не Label то получаем обрабатываем его
			if (!(pnl.getComponent(i) instanceof JLabel)) {
				
				if (pnl.getComponent(i) instanceof DatePicker){
					fieldsParam.put(pnl.getComponent(i).getName(),
							new SimpleDateFormat("dd.MM.yyyy").format(((DatePicker) pnl.getComponent(i)).getDate()));
				}
				if (pnl.getComponent(i) instanceof JTextField){ 
				 fieldsParam.put(pnl.getComponent(i).getName(),((JTextField) pnl.getComponent(i)).getText());
				}
				if (pnl.getComponent(i) instanceof JScrollPane &&
						((JScrollPane) pnl.getComponent(i)).getViewport().getComponent(0) instanceof JTextArea) {
					fieldsParam.put(((JTextArea) ((JScrollPane) pnl.getComponent(i)).getViewport().getComponent(0)).getName(),
							((JTextArea) ((JScrollPane) pnl.getComponent(i)).getViewport().getComponent(0)).getText());
				}
				if (pnl.getComponent(i) instanceof JScrollPane &&
						((JScrollPane) pnl.getComponent(i)).getViewport().getComponent(0) instanceof JPanel){
					
					workInMultiPanel(fieldsParam,pnl.getComponent(i).getName(),
							(JPanel) ((JScrollPane) pnl.getComponent(i)).getViewport().getComponent(0));
				}
				
				if (pnl.getComponent(i) instanceof JTextArea){
					 fieldsParam.put(pnl.getComponent(i).getName(),((JTextArea) pnl.getComponent(i)).getText());					
				}
				if (pnl.getComponent(i) instanceof PanelAndSelectButton){
				    fieldsParam.put(pnl.getComponent(i).getName(),((PanelAndSelectButton) pnl.getComponent(i)).getText());
				}
			}
		}
		return fieldsParam;
	}
	
	// Обрабатывает Панельку с PanelAndSelectButton набором
	private void workInMultiPanel(Map fieldsParam,String keyName,JPanel panel){

		if (panel.getComponentCount()==1){
			fieldsParam.put(keyName, ((PanelAndSelectButton) ((JPanel) panel.getComponent(0)).getComponent(0)).getText());
		}else {
			Map m1= new HashMap();
			int i=0;
			for (Component comp:panel.getComponents()){
				m1.put(i,((PanelAndSelectButton)((JPanel)comp).getComponent(0)).getText());
				i+=1;
			}
			fieldsParam.put(keyName, m1);
		}
	}
	
	class MyOnActionListener implements ActionListener{
		private Component frm;
		private JPanel mpnl;
		public MyOnActionListener(Component frm,JPanel mpnl){
			this.frm=frm;
			this.mpnl=mpnl;
		}

		public void actionPerformed(ActionEvent ae) {
			// TODO Auto-generated method stub
			Component comp = ((JButton) ae.getSource()).getParent().getParent();
			if (ae.getActionCommand().equals("+")){
				((JButton) ae.getSource()).setText("- ");
				
				JTextField field = new JTextField();
				field.setName(((JButton) ae.getSource()).getName());
				JPanel x = new JPanel();
				x.setLayout(new BorderLayout());
				x.add(new PanelAndSelectButton(field,ConstantManager.ONE_ADD),BorderLayout.CENTER);
				JButton addButton = new JButton("+");
				addButton.setName(((JButton) ae.getSource()).getName());
				addButton.addActionListener(new MyOnActionListener(this.frm,this.mpnl));
				x.add(addButton,BorderLayout.EAST);
				((JPanel)comp).add(x);
			}else {
				// удаляем строку
				Component comp2 = ((JButton) ae.getSource()).getParent();
			    ((JPanel) comp).remove(comp2);
			}
			
			this.mpnl.repaint();
			frm.repaint();
			
			((JDialog) frm).pack();
		}
		
	}

}
