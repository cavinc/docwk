package cav.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.tree.TreeSelectionModel;

import cav.models.TemplatePrefModel;
import cav.utils.Dmu;


public class LinkFrm extends JDialog implements ActionListener{
	private JLabel infoDocLabel=new JLabel();
	
	private int w = 500;
	private int h = 400 ;
	public LinkFrm(JFrame frm,ArrayList row,ArrayList<String> fields){
		super(frm);
		this.setTitle("Настройка сваязки полей формы");
		this.setSize(w,h);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		fillTable((Integer) row.get(0),fields);
		fillAllFieldTable(fields);
		
		JPanel upPanel = new JPanel();
		upPanel.setLayout(new BorderLayout());
		upPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		upPanel.add(infoDocLabel,BorderLayout.NORTH);
		upPanel.add(setGrid((Integer) row.get(0)),BorderLayout.CENTER);
		
		this.add(upPanel, BorderLayout.CENTER);
		infoDocLabel.setText("Шаблон : "+row.get(1));
		
		// Панель с кнопками
		JPanel buttonPanel=new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		// Кнопки
		JButton saveButton = new JButton("Сохранить");
		saveButton.addActionListener(this);
		JButton closeButton = new JButton("Отмена");
		closeButton.addActionListener(this);
		
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(saveButton);
		buttonPanel.add(closeButton);
		
		this.add(buttonPanel, BorderLayout.SOUTH);
		
		this.setModal(true);
		this.setResizable(false);
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));
	}
	
	
	private JTable grid;
	private JScrollPane setGrid(int templId){
		grid=new JTable(new TemplatePrefModel(templId));
		grid.setShowGrid(true);
		grid.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		TableColumn typeColumn = grid.getColumnModel().getColumn(2);
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("Строка");
		comboBox.addItem("Текст");
		comboBox.addItem("Число");
		comboBox.addItem("Дата");
		typeColumn.setCellEditor(new DefaultCellEditor(comboBox));
		
		JScrollPane sp = new JScrollPane(grid);
		return sp;
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Отмена")){
			this.dispose();
		}
		if (ae.getActionCommand().equals("Сохранить")){
			saveData();
			this.dispose();
		}
	}
	
	private void saveData(){
		String val;
		for (int i=0;i<((TemplatePrefModel) grid.getModel()).getRowCount();i++){
			ArrayList row = ((TemplatePrefModel) grid.getModel()).getRow(i);
			//System.out.println(row);
			if (row.get(1)!=null){
				val = row.get(1).toString().replaceAll("'", "''");
				String sql="MERGE INTO pref_templ (id,field_name,field_form_name,field_type)"+
			    " values ("+(Integer) row.get(3)+",'"+row.get(0).toString()+"','"+val+"','"+
						row.get(2).toString()+"')";
				Dmu.executeQuery(sql);
				//TODO сделать сохранение только название поля без названия формы и типа 
				/*sql="MERGE INTO field_all_temp (field_name,field_form_name,field_type)"+
					    " values ('"+row.get(0).toString()+"','"+row.get(1).toString()+"','"+
								row.get(2).toString()+"')";
								*/
				sql="MERGE INTO field_all_temp (field_name)"+
					    " values ('"+row.get(0).toString()+"')";
				Dmu.executeQuery(sql);
			}
		}
	}
	
	// заполняет данными табличку 
	private void fillTable(int templId,ArrayList<String> fields){
		for (int i=0;i<fields.size();i++){
			String sql="MERGE INTO pref_templ (id,field_name) values ("+Integer.toString(templId)+",'"+fields.get(i).toString()+"')";
			Dmu.executeQuery(sql);
			
		}
	}
	
	// заполняем данными общую талицу
	private void fillAllFieldTable(ArrayList<String> fields) {
		for (int i=0;i<fields.size();i++){
			String sql="MERGE INTO field_all_temp (field_name) values ('"+fields.get(i).toString()+"')";
			Dmu.executeQuery(sql);			
		}
	}	
}
