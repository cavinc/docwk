package cav.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import cav.models.AllFieldModel;
import cav.utils.ConstantManager;
import cav.utils.Dmu;

public class AllFieldLink extends JDialog implements ActionListener{
	public int w=800;
	public int h=400;
	
	private JFrame frm;
	public AllFieldLink(JFrame frm){
		super(frm);
		this.frm = frm;
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("Определение общих полей");
		this.setSize(w,h);
		this.add(setGrid(),BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.LINE_AXIS));
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		JButton closeButton = new JButton("Закрыть");
		closeButton.addActionListener(this);
		JButton saveButton = new JButton("Сохранить");
		saveButton.addActionListener(this);
		
		JButton insButton = new JButton("Добавить новое");
		insButton.addActionListener(this);
		JButton editButton = new JButton("Редактировать");
		editButton.addActionListener(this);
		JButton valueButton = new JButton("Значения");
		valueButton.addActionListener(this);
		
		
		buttonPanel.add(Box.createHorizontalStrut(25));
		buttonPanel.add(insButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		buttonPanel.add(editButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		buttonPanel.add(valueButton);		
		buttonPanel.add(Box.createHorizontalGlue());		
		//buttonPanel.add(saveButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		buttonPanel.add(closeButton);
		buttonPanel.add(Box.createHorizontalStrut(25));
		
		this.add(buttonPanel,BorderLayout.SOUTH);
		this.setModal(true);
        this.setLocation((this.getToolkit().getScreenSize().width/2)-(this.getSize().width/2),
                (this.getToolkit().getScreenSize().height/2)-(this.getSize().height/2));			
	}

	private JTable grid;
	private JScrollPane setGrid(){
		grid = new JTable(new AllFieldModel());
		grid.setShowGrid(true);
		grid.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		setSizeColumn(grid);
		
		TableColumn typeColumn = grid.getColumnModel().getColumn(2);
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("Строка");
		comboBox.addItem("Текст");
		comboBox.addItem("Число");
		comboBox.addItem("Дата");
		typeColumn.setCellEditor(new DefaultCellEditor(comboBox));		
		
		JScrollPane sp =new JScrollPane(grid);
		return sp;
	}
	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("Закрыть")){
			/*
    		int res = JOptionPane.showConfirmDialog(this,"Вы уверены что хотите выйти ? \n При выходе измененный данные не сохраняться",
    				"Общие поля - настроки",JOptionPane.YES_NO_OPTION);
    		if (res==JOptionPane.YES_OPTION){
    			this.dispose();
    		}
    		*/
			saveData(); // сохраняем все что наблудили при выходе
    		this.dispose();
		}
		/*
		if (ae.getActionCommand().equals("Сохранить")){
			saveData();
			this.dispose();
		}
		*/
		if (ae.getActionCommand().equals("Добавить новое")){
			InsEditAllFieldFrm iEdit = new InsEditAllFieldFrm(this.frm,ConstantManager.INSERT_MODE);
			iEdit.setVisible(true);
			if (iEdit.getSaveFlag()) {
				((AllFieldModel) grid.getModel()).setNewRow(iEdit.getData());
				saveData(); //TODO переделать на сохранение не всего массива а отдельной записи
			}else {
				((AllFieldModel) grid.getModel()).refresh();
			}
		}
		if (ae.getActionCommand().equals("Редактировать")){
			int row_id = grid.getSelectedRow();
			if (row_id!=-1){
				saveData(); // сохраняем все что наблудили при входе
				InsEditAllFieldFrm iEdit = new InsEditAllFieldFrm(this.frm,
						ConstantManager.EDIT_MODE,((AllFieldModel) grid.getModel()).getRow(row_id));
				iEdit.setVisible(true);
				if (iEdit.getSaveFlag()) {
					((AllFieldModel) grid.getModel()).setRow(iEdit.getData(),row_id);
					saveData();
				}else {
					((AllFieldModel) grid.getModel()).refresh();
				}			
			}
		}
		
		if (ae.getActionCommand().equals("Значения")){
			int row_id = grid.getSelectedRow();
			if (row_id!=-1){
				ArrayList data = ((AllFieldModel) grid.getModel()).getRow(row_id);
				if (data.get(2)!=null){
					AllFieldValue afv = new AllFieldValue(this.frm,(String) data.get(0),(String) data.get(2),ConstantManager.EDIT_MODE_SPEC);
					afv.setVisible(true);
					if (afv.saveFlg) {
						ArrayList spec_data = afv.getData();
						saveSpecData(spec_data,(String)data.get(0));
						data.set(3, spec_data.size());
						((AllFieldModel) grid.getModel()).setRow(data,row_id);
						saveData();
					}
				}else {
					JOptionPane.showMessageDialog(this,"Не указан тип поля !");
				}
			} else {
				JOptionPane.showMessageDialog(this,"Не выбран элемент для которого устанавливаем значения");
			}
		}
		
	}
	
	private void saveData(){
		String val;
		for (int i=0;i<((AllFieldModel) grid.getModel()).getRowCount();i++){
			ArrayList row = ((AllFieldModel) grid.getModel()).getRow(i);
			val = row.get(1).toString().replaceAll("'","''");
			/*
			if (row.get(1)!=null && row.get(3)!=null){
				String sql="MERGE INTO field_all_temp (field_name,field_form_name,field_type,field_source)"+
			" values ('"+row.get(0).toString()+"','"+row.get(1).toString()+"','"+row.get(2).toString()+"','"+row.get(3).toString()+"')";
				Dmu.executeQuery(sql);
				continue;
			}
			*/
			if (row.get(1)!=null && row.get(2)!=null){
				String sql="MERGE INTO field_all_temp (field_name,field_form_name,field_type,spec_count)"+
			" values ('"+row.get(0).toString()+"','"+val+"','"+row.get(2).toString()+"',"+row.get(3).toString()+")";
				Dmu.executeQuery(sql);
				continue;				
			}
			
			if (row.get(1)!=null){
				String sql="MERGE INTO field_all_temp (field_name,field_form_name,spec_count)"+
			" values ('"+row.get(0).toString()+"','"+val+"',"+row.get(3).toString()+")";
				Dmu.executeQuery(sql);					
			}			
		}
	}
	
	// сохраняем спецификацию
	private void saveSpecData(ArrayList data,String fieldName){
		String sql= "delete from field_all_spec where field_name = '"+fieldName+"'";
		Dmu.executeQuery(sql);
		String val;
		for (int i=0;i<data.size();i++){
			val = ((ArrayList)data.get(i)).get(1).toString().replaceAll("'","''");
			sql="MERGE INTO field_all_spec (field_name,id_rec,field_source) "+
		    "values ('"+fieldName+"',"+((ArrayList)data.get(i)).get(0)+",'"+val+"')";
			Dmu.executeQuery(sql);
		}
	}
	// устанавливает размер колонки
	private void setSizeColumn(JTable grid){
		TableColumnModel model = grid.getColumnModel();
		//model.getColumn(0).setMaxWidth(100);
    	//model.getColumn(0).setMinWidth(100);
	  model.getColumn(2).setMaxWidth(100);
	  model.getColumn(2).setMinWidth(100);
	}
	
}
