package cav.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreeSelectionModel;

import cav.models.TemplateModel;
import cav.utils.ConstantManager;
import cav.utils.Dmu;
import cav.utils.Glu;


public class MainFrm extends JFrame implements ActionListener,MouseListener {
	private int w=this.getToolkit().getScreenSize().width;
	
	private int h=this.getToolkit().getScreenSize().height-32;
	
	public MainFrm (){
		super();
		this.setTitle("Управление шаблонами");
		this.setSize(w,h);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setJMenuBar(setMenu());
		this.setLayout(new BorderLayout());
		this.add(setToolbox(),BorderLayout.NORTH);
		this.add(setGrid(),BorderLayout.CENTER);
		
		StatusBar sb=new StatusBar();
		this.add(sb,BorderLayout.SOUTH);
		setSizeColumn(grid);
	}
	
	private void setItem(JMenu m,String prm){
		String[] x=prm.split(";");
		for (int i=0;i<x.length;i++){
			String[] xx = x[i].split(":");
			if (xx[0].equals("-")){
				m.addSeparator();
			}else {
				JMenuItem mItem = new JMenuItem(xx[0]);
				mItem.setName(xx[1]);
				mItem.addActionListener(this);
				m.add(mItem);
			}
		}
		
	}
	
	private JMenuBar mb;
	private JMenuBar setMenu(){
		mb=new JMenuBar();
		JMenu function = new JMenu ("Служебные функции");
		setItem(function,"Создать шаблон:create_template;Конфигуратор:config;-:-;Редактировать общие поля:editallfield");
		JMenu about=new JMenu("?");
		setItem(about,"О программе:about");
		mb.add(function);
		mb.add(about);
		return mb;
	}
	
	private JToolBar setToolbox(){
		JToolBar tb=new JToolBar();
		String sb="{printer.png,Печать...,print};{-,-,-};{note_add.png,Добавить,instemplate}";
		sb=sb+";{edit.png,Редактировать,edittemplate};{-,-,-};{delete.png,Удалить,deltemplate}";
		
		String[] bb=sb.split(";");
		for (int i=0;i<bb.length;i++){
			bb[i]=bb[i].replace("{","");
			bb[i]=bb[i].replace("}","");
			String s=(((bb[i]).toString()).split(",")[0]);
			if (s.equals("-")){
				tb.addSeparator();	
			}else {
				URL url = MainFrm.class.getResource("/resources/"+s);
				//System.out.println(url);
						
				JButton bt=new JButton(((bb[i]).toString()).split(",")[1]);
				if (url!=null){
					bt.setIcon(new ImageIcon(url));
				}
				s=((bb[i]).toString()).split(",")[2];
				bt.setName(s);
				bt.setToolTipText(((bb[i]).toString()).split(",")[1]);
				bt.addActionListener(this);
				tb.add(bt);
			}
		}
		return tb;
	}
	private JTable grid;
	private JScrollPane setGrid(){
		grid = new JTable(new TemplateModel());
		grid.setShowGrid(true);
		//grid.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		grid.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		grid.addMouseListener(this);
		
		JScrollPane sp=new JScrollPane(grid);
		return sp;
	}

	public void actionPerformed(ActionEvent ae) {
		// TODO переделать 
    
	    if (ae.getActionCommand().equals("Добавить")){
	    	insertTemplate();
	    }
	    if (ae.getActionCommand().equals("Редактировать")){
	    	int row_id= grid.getSelectedRow();
	    	if (row_id==-1){
	    		JOptionPane.showMessageDialog(this, "Не выбран шаблон");
	    	} else {
	    		ArrayList row = ((TemplateModel) grid.getModel()).getRow(row_id);
	    		editTemplate(row);
	    	}
	    }

	    if (ae.getActionCommand().equals("Удалить")){
	    	int row_id = grid.getSelectedRow();
	    	if (row_id!=-1) {
	    		int res = JOptionPane.showConfirmDialog(this,"Вы уверены что хотите удалить ?","Шаблоны - удаление",JOptionPane.YES_NO_OPTION);
	    		if (res==JOptionPane.YES_OPTION){
	    			// удаляем
	    			ArrayList row = ((TemplateModel) grid.getModel()).getRow(row_id);
	    			String sql=ConstantManager.DELETE_TEMPLATE+row.get(0).toString();
	    			Dmu.executeQuery(sql);
	    			((TemplateModel) grid.getModel()).refresh();
	    		}
	    	}
	    }
	    
	    if (ae.getActionCommand().equals("Конфигуратор")){
			ConfigFrm config=new ConfigFrm();
			config.setVisible(true);	    	
	    }
	    
	    if (ae.getActionCommand().equals("Создать шаблон")){
	    	//JasperFrm design = new JasperFrm();
	    	//design.setVisible(true);
	    	if (Glu.getCFG("IREPORT_PATH").length()!=0){
	    		try{
	    			//Process p = Runtime.getRuntime().exec(Glu.getCFG("IREPORT_PATH"));
	    			//p.waitFor();
	    			Runtime.getRuntime().exec(Glu.getCFG("IREPORT_PATH"));
	    		}catch(Exception e) {
	    			JOptionPane.showMessageDialog(this, "Ошибка "+e.getLocalizedMessage());
	    		}
	    	}else {
	    		JOptionPane.showMessageDialog(this, "Не указан путь к исполняемому файлу iReport");
	    	}
	    }
	    
	    if (ae.getActionCommand().equals("Печать...")){
	    	int row_id = grid.getSelectedRow();
	    	if (row_id!=-1) {
	    		ArrayList row = ((TemplateModel) grid.getModel()).getRow(row_id);
	    		DataPrintFrm dpFrm = new DataPrintFrm(this,row);
	    		dpFrm.setVisible(true);
	    	}else {
	    		JOptionPane.showMessageDialog(null, "Не выбрано ни одного шаблона");
	    	}
	    }
	    
	    if (ae.getActionCommand().equals("О программе")){
	    	AboutFrm about = new AboutFrm(this);
	    	about.setVisible(true);
	    }
	    
	    if (ae.getActionCommand().equals("Редактировать общие поля")){
	    	AllFieldLink afield = new AllFieldLink(this);
	    	afield.setVisible(true);
	    }
	    
	}
	
	private void insertTemplate(){
		InsEditTemplateFrm ieTempl = new InsEditTemplateFrm(this,ConstantManager.INSERT_MODE);
		ieTempl.setVisible(true);
		ieTempl.dispose();
		((TemplateModel) grid.getModel()).refresh();
	}
	
	private void editTemplate(ArrayList row){
		InsEditTemplateFrm ieTempl = new InsEditTemplateFrm(this,ConstantManager.EDIT_MODE,row);
		ieTempl.setVisible(true);
		ieTempl.dispose();
		((TemplateModel) grid.getModel()).refresh();		
	}

	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	// устанавливает размер колонки
	private void setSizeColumn(JTable grid){
		TableColumnModel model = grid.getColumnModel();
		model.getColumn(0).setMaxWidth(100);
		model.getColumn(0).setMinWidth(100);
		model.getColumn(2).setMaxWidth(150);
		model.getColumn(2).setMinWidth(150);
	}

}
