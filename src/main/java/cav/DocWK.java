package cav;

import java.io.File;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import cav.ui.ConfigFrm;
import cav.ui.MainFrm;
import cav.utils.Dmu;
import cav.utils.Glu;

public class DocWK {
	public static void main (String args[]){
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.put("OptionPane.yesButtonText", "Да");
            UIManager.put("OptionPane.noButtonText", "Нет");
            UIManager.put("OptionPane.cancelButtonText", "Отмена");
            UIManager.put("OptionPane.okButtonText", "Готово");			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
		if (Glu.init()){
			Glu.loadCFG(Glu.getSession("UserHome")+File.separator+".docwk"+File.separator+"docwk.conf");
		}else {
			ConfigFrm config=new ConfigFrm();
			config.setVisible(true);
		}
		Dmu.OpenDB();
		Dmu.TestAndCreateTable();
		
		MainFrm frm = new MainFrm();
		frm.setVisible(true);
	}

}
