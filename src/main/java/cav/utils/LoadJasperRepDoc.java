package cav.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

public class LoadJasperRepDoc {
	private ArrayList<String> mFields =new ArrayList<String>() ;
	
	public LoadJasperRepDoc(String fname){
		 InputStream is=null;
		 try{
           //  try {
           //      is=new FileInputStream(fname);
           //  } catch (FileNotFoundException e) {
           //      e.printStackTrace();
           //  }			 
			 JasperReport jasperReport = JasperCompileManager.compileReport(fname);
			/// JasperReport jasperReport = JasperCompileManager.compileReport(is);
			// JRParameter[] par = jasperReport.getParameters();
			 
			for (JRParameter param :jasperReport.getParameters()){
			//	System.out.println("NAME :"+param.getName());
			//	System.out.println("DESC :"+param.getDescription());
			//	System.out.println("SYS DEF :"+param.isSystemDefined());
			//	System.out.println("CLASS :"+param.getValueClassName());
				if (!param.isSystemDefined()) {
					mFields.add(param.getName());
				}
			}
			 
		 }catch (Exception e){
			 e.printStackTrace();
		 }
		
	}
	
	public ArrayList<String> getFieldDoc(){
		return mFields;
	}

}
