package cav.utils;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.michaelbaranov.microba.calendar.DatePicker;

import cav.ui.AllFieldValue;

public class PanelAndSelectButton extends JPanel {
	
	private JComponent component;
	
	private int modeAdd;
	
	public PanelAndSelectButton(final JComponent component,final int modeAdd){
		super();
		this.setLayout(new BorderLayout());
		this.add(component,BorderLayout.CENTER);
		this.component=component;
		this.modeAdd=modeAdd;
		this.setName(component.getName());
		JButton selectButton = new JButton("...");
		selectButton.setToolTipText("Выбрать значение");
		//selectButton.addActionListener(lisneter);
		selectButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				AllFieldValue afv = new AllFieldValue(null, component.getName(), "",ConstantManager.SELECT_MODE_SPEC);
				afv.setVisible(true);
				if (afv.selectFlg){
					ArrayList data=afv.getSelectData();
					if (component instanceof JTextField) {
						if (modeAdd == ConstantManager.ONE_ADD) {
							((JTextField) component).setText(data.get(1).toString());
						}else {
							((JTextField) component).setText(((JTextField) component).getText()+" "+data.get(1).toString());
						}
					}
					if (component instanceof JTextArea) {
						((JTextArea) component).setText(data.get(1).toString());
					}
					if (component instanceof DatePicker) {
						try {
							((DatePicker) component).setDate((new SimpleDateFormat("dd.MM.yyyy")).parse(data.get(1).toString()));
						} catch (PropertyVetoException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					if (component instanceof JScrollPane) {
						if (modeAdd == ConstantManager.ONE_ADD) {
							((JTextArea)((JScrollPane) component).getViewport().getComponent(0)).setText(data.get(1).toString());
						}
					}
				}
			}});
		//selectButton.setName(component.getName());
		this.add(selectButton,BorderLayout.EAST);
	}
	
	public String getText(){
		String data = null;
		if (component instanceof JTextField){
			data = ((JTextField) component).getText();
		}
		if (component instanceof DatePicker){
			data = new SimpleDateFormat("dd.MM.yyyy").format(((DatePicker) component).getDate());
		}
		if (component instanceof JTextArea) {
			data = ((JTextArea) component).getText();
		}
		if (component instanceof JScrollPane) {
			data = ((JTextArea)((JScrollPane) component).getViewport().getComponent(0)).getText();
		}
		return data;
	}

}
