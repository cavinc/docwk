package cav.utils;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CustomCellRender extends DefaultTableCellRenderer  {
	private int buttonColumnIndex;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		if (buttonColumnIndex==column){
			JButton btn = new JButton("...");
			return btn;
		}else {
			return super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
				row, column);
		}
	}
	
}
