package cav.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;

public class ManagerTemplatePrint {
	
	public static void printTemplate(String fname,Map map){
		// DOCX 
		String retFname = null;
		if (fname.toUpperCase().indexOf(".DOCX")!=-1 || fname.toUpperCase().indexOf(".DOC")!=-1){
			try {
				SaveAndPritnDocX spd = new SaveAndPritnDocX(fname,map);
				File fl = new File(fname);
				retFname=spd.saveDocX(Glu.getCFG("TEMPLATE_OUT_DIR")+File.separator+fl.getName());
				JOptionPane.showMessageDialog(null, "Готовый файл сохранент в каталог \n"+Glu.getCFG("TEMPLATE_OUT_DIR"));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
			}
			
			
			if (System.getProperty("os.name").equals("Linux") && retFname !=null){
				try {
					File fl = new File(fname);
					Process p = Runtime.getRuntime().exec("lowriter "+retFname);
					p.waitFor();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
				}				
			}
			
			if (System.getProperty("os.name").indexOf("Windows")!=-1 && retFname !=null){
				if (Glu.getCFG("OFFICE_BIN_PATH").length()!=0){
					try {
						File fl = new File(fname);
						Process p = Runtime.getRuntime().exec(Glu.getCFG("OFFICE_BIN_PATH")+" "+retFname);
						p.waitFor();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
					}
				} else {
					JOptionPane.showMessageDialog(null,"Не указан путь к исполняемогу файлу офиса");
				}
			}

		}

		// Source JascperReport
		if (fname.toUpperCase().indexOf(".JRXML")!=-1){
			Iterator<String> it = map.keySet().iterator();
			while (it.hasNext()) {
			    String key = it.next();
			    if (map.get(key) instanceof Map){
			    	Map m = (Map) map.get(key);
			    	Iterator<Integer> itr = m.keySet().iterator();
			    	StringBuilder val = new StringBuilder();
			    	while (itr.hasNext()){
			    		val.append(" ");
			    		val.append(m.get(itr.next()));
			    	}
			    	map.put(key, val.toString());
			    }
			}
			
			JasperPrintFrm jpf = new JasperPrintFrm(null,fname,map);
			jpf.printDialog();
		}
	}

}
