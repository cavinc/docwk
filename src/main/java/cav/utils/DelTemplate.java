package cav.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.h2.api.Trigger;

public class DelTemplate implements Trigger {

	public void close() throws SQLException {
		// TODO Auto-generated method stub
		
	}
    // обработка тригера
	public void fire(Connection conn, Object[] oldRow, Object[] newRow) throws SQLException {
		// TODO Auto-generated method stub
		PreparedStatement stmt = conn.prepareStatement("delete from spec_templ where id=?");
		stmt.setObject(1,oldRow[0]);
		stmt.executeUpdate();
		stmt = conn.prepareStatement("delete from pref_templ where id=?");
		stmt.setObject(1,oldRow[0]);
		stmt.executeUpdate();
		
		stmt = conn.prepareStatement("delete from spec_templ_extend where id=?");
		stmt.setObject(1,oldRow[0]);
		stmt.executeUpdate();		
	}

	public void init(Connection arg0, String arg1, String arg2, String arg3, boolean arg4, int arg5)
			throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void remove() throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
