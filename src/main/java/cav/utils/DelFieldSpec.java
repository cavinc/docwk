package cav.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.h2.api.Trigger;

public class DelFieldSpec implements Trigger {

	public void close() throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void fire(Connection conn, Object[] oldRow, Object[] newRow)
			throws SQLException {
		PreparedStatement stmt = conn.prepareStatement("delete from field_all_spec where field_name=?");
		stmt.setObject(1,oldRow[0]);
		stmt.executeUpdate();
	}

	public void init(Connection arg0, String arg1, String arg2, String arg3,
			boolean arg4, int arg5) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	public void remove() throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
