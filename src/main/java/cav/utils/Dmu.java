package cav.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

/**
 * 
 * @author Kotov Alexandr
 *
 */

public class Dmu {
	private static Connection conn=null;
	private static Statement st = null;
	private static Statement st2 = null;
	
	public static void OpenDB(){
		try {
			Class.forName("org.h2.Driver").newInstance();
			Connection conn = DriverManager.getConnection("jdbc:h2:./templatedb","sa", "");
			st = conn.createStatement();
			st2 = conn.createStatement();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Ошибка при открытии базы :\n" + e.getLocalizedMessage());
		} 
	}
	public static void CloseDB(){
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Ошибка при закрытии базы) :\n" + e.getLocalizedMessage());
		}
	}
	
	public static ResultSet getQueryRes(String sql){
		ResultSet rs=null;
		if (sql.length()!=0){
            try {
                rs=st.executeQuery(sql);
            } catch (SQLException e) {
               // e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Ошибка при выполнении операции (ResultSet) :\n" + e.getLocalizedMessage());
            }			
		}
		return rs;
	}
	
	public static ResultSet getQueryResTwo(String sql){
		ResultSet rs=null;
		if (sql.length()!=0){
            try {
                rs=st2.executeQuery(sql);
            } catch (SQLException e) {
               // e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Ошибка при выполнении операции (ResultSet) :\n" + e.getLocalizedMessage());
            }			
		}
		return rs;
	}	
	
	public static void executeQuery(String sql){
        if (sql.length()!=0){
            try {
                st.execute(sql);
            } catch (SQLException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Ошибка при выполнении операции :\n" + e.getLocalizedMessage());
            }
        }
	}
	
	public static void TestAndCreateTable(){
		try {
			String sql="CREATE TABLE IF NOT EXISTS templ (id integer not null primary key,templ_name varchar(127),"
					+ "modify_date date,templ_path varchar(256));";			
			st.execute(sql);
			
			//sql="drop table pref_templ";
			//st.execute(sql);
			// описание полей шаблона
			sql="CREATE TABLE IF NOT EXISTS pref_templ (id integer not null,field_name varchar(80),"+
			"field_form_name varchar(80),field_type varchar(20), primary key(id,field_name));";
			st.execute(sql);
			// спецификация полей шаблона
			sql="CREATE TABLE IF NOT EXISTS spec_templ (id integer not null,field_name varchar(80) not null,"+
			"field_source varchar(1024),field_type varchar(20),primary key(id,field_name));";
			st.execute(sql);
			// спецификация с дополнительными полями шаблона
			sql="CREATE TABLE IF NOT EXISTS spec_templ_extend (id integer not null,field_name varchar(80) not null,"+
			"post_id integer not null,field_source varchar(1024), primary key(id,field_name,post_id))";
			st.execute(sql);
			
			// тригер на удаление
			sql="CREATE TRIGGER  IF NOT EXISTS delTempl AFTER DELETE ON templ "+
			"FOR EACH ROW call \"cav.utils.DelTemplate\"";	
			//sql="DROP TRIGGER delTempl"; 
			st.execute(sql);
			
			
		    // список общих полей и значений 
			sql = "CREATE TABLE IF NOT EXISTS field_all_temp (field_name varchar(80),"+
		    "field_form_name varchar(80),field_type varchar(20),spec_count integer default 0, primary key(field_name));";
			st.execute(sql);
			
			// список значений общих полей  
			sql = "CREATE TABLE IF NOT EXISTS field_all_spec (field_name varchar(80) not null,id_rec integer not null,"+
			"field_source varchar(1024),primary key(field_name,id_rec))";
			st.execute(sql);
			
			// тригер на удаление связанных полей
			sql = "CREATE TRIGGER IF NOT EXISTS delField AFTER DELETE ON field_all_temp "+
			"FOR EACH ROW call \"cav.utils.DelFieldSpec\"";
			st.execute(sql);
				

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Ошибка при создании таблиц :\n" + e.getLocalizedMessage());
		}
		
		
		
	}

}
