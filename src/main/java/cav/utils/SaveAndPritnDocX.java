package cav.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class SaveAndPritnDocX {
	private Map map;
	public SaveAndPritnDocX(String fname,Map map) throws FileNotFoundException{
		this.map=map;
		FileInputStream ins=new FileInputStream(fname);
		if (fname.toUpperCase().indexOf(".DOCX")!=-1){
			loadAndParseDocX(ins);
		}else if (fname.toUpperCase().indexOf(".DOC")!=-1){
			loadAndParseDoc(ins);
		}else {
			JOptionPane.showMessageDialog(null, "Данных формат пока не подерживается");
		}		
	}
	private XWPFDocument docX2;
	private void loadAndParseDocX(FileInputStream ins){
		try {
			docX2=new XWPFDocument(ins);
			ins.close();
			for (XWPFParagraph par : docX2.getParagraphs()){
				 replaceField(par,this.map);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
		}			
	}
	
	private HWPFDocument doc;
	private void loadAndParseDoc(FileInputStream ins){
		try {
			doc = new HWPFDocument(ins);
			ins.close();
			Range range = doc.getOverallRange();
			replaceFieldDoc(range,this.map);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
		}
	}
	
	private void replaceFieldDoc(Range range,Map map){
		// http://mail-archives.apache.org/mod_mbox/poi-user/200908.mbox/%3C24876942.post@talk.nabble.com%3E
		Set<Map.Entry<String, Object>> entrySet = map.entrySet();
		for (Entry<String, Object> entry : entrySet){
			if (entry.getValue() instanceof String) {
				range.replaceText("{{"+entry.getKey()+"}}",(String) entry.getValue());
			}
			if (entry.getValue() instanceof Map) {
				Set<Map.Entry<Integer,String>> entryM1 = ((Map) entry.getValue()).entrySet();
				//String val="";
				StringBuilder val = new StringBuilder();
				for (Entry<Integer,String> enr: entryM1){
					//val=val +" "+ enr.getValue().replaceAll("'","''");
					val.append(" ");
					val.append(enr.getValue().replaceAll("'","''"));
				}
				range.replaceText("{{"+entry.getKey()+"}}",val.toString());
			}
		}
	}
	
	private void replaceField(XWPFParagraph paragraph,Map map){
		String s = paragraph.getParagraphText();
		//Pattern p = Pattern.compile("(\\{\\{[a-zA-Z_\\d]+\\}\\})");
		//Matcher m = p.matcher(s); 
		
		Set<Map.Entry<String, Object>> entrySet = map.entrySet();
		for (Entry<String, Object> entry : entrySet){
			//System.out.println(entry.getKey());
			if (entry.getValue() instanceof String) {
				Matcher m = Pattern.compile("(\\{\\{["+entry.getKey()+"]+\\}\\})").matcher(s);
				if (m.find()) {
					s=s.replaceAll("\\{\\{["+entry.getKey()+"]+\\}\\}", (String) entry.getValue());
					List<XWPFRun> pp = paragraph.getRuns();
					for (XWPFRun p:pp){
						p.setText(s,0);
					}
				}				
			}
			if (entry.getValue() instanceof Map) {
				Set<Map.Entry<Integer,String>> entryM1 = ((Map) entry.getValue()).entrySet();
				StringBuilder val = new StringBuilder();
				for (Entry<Integer,String> enr: entryM1){
					val.append(" ");
					val.append(enr.getValue().replaceAll("'","''"));					
				}
				Matcher m = Pattern.compile("(\\{\\{["+entry.getKey()+"]+\\}\\})").matcher(s);
				if (m.find()) {
					s=s.replaceAll("\\{\\{["+entry.getKey()+"]+\\}\\}", val.toString());
					List<XWPFRun> pp = paragraph.getRuns();
					for (XWPFRun p:pp){
						p.setText(s,0);
					}
				}				
			}
		}
	}
	// добавляет к файлу временную метку и сохраняет его
	public String saveDocX(String fname){
		String dateS = new SimpleDateFormat("yyyy-MM-ddHHmm").format(new Date());
		fname = fname.replace(".", dateS+".");
		FileOutputStream out;
		try {
			out = new FileOutputStream(fname);
			
			if (fname.toUpperCase().indexOf(".DOCX")!=-1){
				docX2.write(out);
			}else if (fname.toUpperCase().indexOf(".DOC")!=-1){
				doc.write(out);
			}			
			out.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
			return null;
		}
		return fname;

	}

}
