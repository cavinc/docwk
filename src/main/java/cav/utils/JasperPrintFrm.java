package cav.utils;

import java.awt.BorderLayout;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JFrame;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;

public class JasperPrintFrm {
	private JRViewer jv;
	private JasperPrint jasperPrint;
	
	private JFrame frm;
	
	public JasperPrintFrm(JFrame frm,String fname,Map params){
		this.frm=frm;
		try{
			if (fname.toLowerCase().indexOf(".jrxml")!=-1){
				InputStream is=null;
                try {
                    is=new FileInputStream(fname);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                JasperReport jasperReport = JasperCompileManager.compileReport(is);
                // JREmptyDataSource() - нужен что бы формировалсая страница 
                jasperPrint = JasperFillManager.fillReport(jasperReport, params,new JREmptyDataSource());
                jv =new JRViewer(jasperPrint);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void printDialog(){
        JDialog pFrm=new JDialog(this.frm,"Печать документа");
        pFrm.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pFrm.setSize(800,600);
        pFrm.getContentPane().add(jv, BorderLayout.CENTER);
        pFrm.validate();
        pFrm.setAlwaysOnTop(true);
        pFrm.setModal(true);
        pFrm.setVisible(true);		
	}

}
