package cav.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class ManagerTemplateLoad {
	
	// возвращаем набор полей
	public static ArrayList<String> getArrayField(String fname){
		// DOCX 
		if (fname.toUpperCase().indexOf(".DOCX")!=-1){
			LoadDocXFile ldx = null;
			try {
				ldx= new  LoadDocXFile(fname);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
			}
			return ldx.getFieldDoc();
		}
		// DOC
		if (fname.toUpperCase().indexOf(".DOC")!=-1){
			LoadDocXFile ldc = null;
			try {
				ldc = new LoadDocXFile(fname);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());				
			}
			return ldc.getFieldDoc();
		}
		// Source JascperReport
		if (fname.toUpperCase().indexOf(".JRXML")!=-1){
			LoadJasperRepDoc ljsdoc = new LoadJasperRepDoc(fname);
			return ljsdoc.getFieldDoc();
		}
		return null;
	}

}
