package cav.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import org.apache.poi.POIXMLProperties;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;





public class LoadDocXFile {
	
	private ArrayList<String> mFields =new ArrayList<String>() ;
	
	public LoadDocXFile(String fname) throws FileNotFoundException{
		 
		FileInputStream ins=new FileInputStream(fname);
		if (fname.toUpperCase().indexOf("DOCX")!=-1){
			loadAndParseDocX(ins);
		}else if(fname.toUpperCase().indexOf("DOC")!=-1){
			loadAndParseDoc(ins);
		} else {
			JOptionPane.showMessageDialog(null, "Данных формат пока не подерживается");
		}
		
	}
	
	
	private void loadAndParseDocX(FileInputStream ins){
		try {
			XWPFDocument docX2=new XWPFDocument(ins);
			
			POIXMLProperties propirtes = docX2.getProperties();
			//System.out.println(propirtes.getCoreProperties().getDescription());
			List<XWPFParagraph> par = docX2.getParagraphs();
			
			 for (XWPFParagraph paragraph : par) {
				 String  s = paragraph.getParagraphText();
				 parseField(s);
			 }
			
			/*XWPFParagraph[] par = docX2.getParagraphs();
			for (int i=0;i<par.length;i++){
		       // System.out.println("Paragraf :"+Integer.toString(i));
			  //  System.out.println(par[i].getParagraphText());
				String s = par[i].getParagraphText();
				parseField(s);
			}
			*/
			ins.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
		}		
	}
	
	private void loadAndParseDoc(FileInputStream ins){
		try {
			HWPFDocument doc = new HWPFDocument(ins);
			 Range range = doc.getRange();
			 parseField(range.text());
			ins.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,"Ошибка :"+e.getLocalizedMessage());
		}
	}
		
	public ArrayList<String> getFieldDoc(){
		return mFields;
	}
	
	private void parseField(String g){
		// TODO причесать потом
		Pattern p = Pattern.compile("(\\{\\{[a-zA-Z_\\d]+\\}\\})");
		Matcher m = p.matcher(g); 
		while (m.find()){
			//System.out.println("field: " + m.group());
			//System.out.println(m.start()+":: "+m.end());
		    String s = m.group();
		    s=s.replace("{{", "").replace("}}", "").trim();
		    mFields.add(s);
		}
		
	}

}
