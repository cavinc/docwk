package cav.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

//https://github.com/opensagres/xdocreport - либа для работы с docx и ods
public class Glu {
	private static Properties sessionProp;
	private static Properties prop;// основные настройки системы
	
    public static void setCFG(String key,String val){
        prop.setProperty(key,val);
    }
    public static String getCFG(String key){
    	if (!prop.containsKey(key)) {
    		return "";
    	}
        return prop.getProperty(key);
    }
    
    public static String getSession(String key){
        return sessionProp.getProperty(key);
    }
    public static void setSession(String key,String val){
        sessionProp.setProperty(key,val);
    }
    
    public static void testAndCreatePath(String path){
        File fn=new File(path);
        if (!fn.exists()){
            fn.mkdir();
        }
    } 
    
    public static boolean init(){
        prop=new Properties();
        prop.clear();
        sessionProp=new Properties();
        sessionProp.clear();
        sessionProp.setProperty("UserHome",System.getProperty("user.home"));
        testAndCreatePath(sessionProp.getProperty("UserHome")+File.separator+".docwk");
        File fn=new File(sessionProp.getProperty("UserHome")+File.separator+".docwk"+File.separator+"docwk.conf");
        if (!fn.exists()) return false;
        return true;
    }
    
    public static void loadCFG(String fname){
        prop.clear();
        try{
            FileInputStream os=new FileInputStream(fname);
            prop.loadFromXML(os);
            os.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.gc();
    }
    public static void saveCFG(String fname) throws IOException {
        FileOutputStream os=new FileOutputStream(fname);
        prop.storeToXML(os,"Последняя конфигурация :"+new Date(),"UTF-8");
        os.close();
        System.gc();
    } 
    // Возвращает номер последней (максимальной) записи в списке шаблонов
    public static int getLastDocID(){
    	int ret=-1;
    	String sql="select COALESCE(max(id),0) as ci from templ";
    	ResultSet rs=Dmu.getQueryRes(sql);
    	try {
			while(rs.next()){
				ret=rs.getInt("ci");
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return ret;
    }
    
	// заполняет данными табличку 
	public static void fillTable(int templId,ArrayList<String> fields){
		for (int i=0;i<fields.size();i++){
			String sql="MERGE INTO pref_templ (id,field_name) values ("+Integer.toString(templId)+",'"+fields.get(i).toString()+"')";
			Dmu.executeQuery(sql);
		}
	}

}
